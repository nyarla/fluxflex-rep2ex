<?php

/**
 * iPhoneのリモートホスト正規表現とIPアドレス帯域 (2010/08/28 時点)
 * Powerd by パンダウォッチャー
 *
 * @link http://panda-watcher.blogspot.com/
 */

$reghost = '/\\.panda-world\\.ne\\.jp$/';

$bands = array(
    '126.196.0.0/14',
    '126.200.0.0/13',
    '126.208.0.0/12',
);

/*
 * Local Variables:
 * mode: php
 * coding: cp932
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
// vim: set syn=php fenc=cp932 ai et ts=4 sw=4 sts=4 fdm=marker:
