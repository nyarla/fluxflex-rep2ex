<?php
/**
 * PHP interface of Hyper Estraier
 *
 * A porting of estraierpure.rb which is a part of Hyper Estraier.
 *
 * Hyper Estraier is a full-text search system. You can search lots of
 * documents for some documents including specified words. If you run a web
 * site, it is useful as your own search engine for pages in your site.
 * Also, it is useful as search utilities of mail boxes and file servers.
 *
 * PHP version 4
 *
 * Copyright (c) 2005-2007 Ryusuke SEKIYAMA. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any personobtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @copyright   2005-2007 Ryusuke SEKIYAMA
 * @license     http://www.opensource.org/licenses/mit-license.php  MIT License
 * @version     SVN: $Id:$
 * @link        http://page2.xrea.jp/  (Project web site)
 * @link        http://hyperestraier.sourceforge.net/  (Hyper Estraier)
 * @since       File available since Release 0.0.1
 * @filesource
 */

// {{{ load dependencies

require_once 'PEAR/ErrorStack.php';

// }}}
// {{{ constants

/**
 * The version number of EstraierPure.
 */
define('ESTRAIERPURE_VERSION', '0.6.0');

/**
 * Error codes.
 */
define('ESTRAIERPURE_ERROR_INVALID_ARGUMENT',     -1);
define('ESTRAIERPURE_ERROR_CONNECTION_FAILED',    -2);
define('ESTRAIERPURE_ERROR_CONNECTION_TIMEDOUT', -3);
define('ESTRAIERPURE_ERROR_MALFORMED_RESPONSE',   -4);

/**
 * Specifies debug mode.
 *
 * If set to `1', every methods check their argument datatype.
 */
if (!defined('ESTRAIERPURE_DEBUG')) {
    define('ESTRAIERPURE_DEBUG', 0);
}

// }}}
// {{{ class EstraierPure

/**
 * Class for simple document handling and searching.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.6.0
 * @static
 */
class EstraierPure
{
    // {{{ private methods

    /**
     * Get a node object.
     *
     * @param   string  $url    The url of a node server.
     *                          Also includes the username and the password.
     * @return  object  EstraierPure_Node
     *                  A node object.
     *                  On error, returns `null'.
     * @access  private
     * @static
     * @ignore
     */
    function &get_node($url)
    {
        static $node = null;
        static $checksum = '';

        // parse the url
        if (!is_string($url)) {
            trigger_error(sprintf(
                    'Argument#2 should be a kind of integer or string, %s given.',
                    gettype($id)), E_USER_WARNING);
            $err = null;
            return $err;
        }
        if (!($purl = @parse_url($url)) ||
            !isset($purl['scheme']) || strcasecmp($purl['scheme'], 'http') != 0 ||
            !isset($purl['host']) || !isset($purl['path']) ||
            (isset($purl['user']) xor isset($purl['pass'])))
        {
            trigger_error('Invalid URL given.', E_USER_WARNING);
            $err = null;
            return $err;
        }

        // check if the node object is cached
        $newchecksum = md5($url);
        if ($checksum != $newchecksum) {
            $node = &new EstraierPure_Node;
            $nurl = 'http://' . $purl['host'];
            if (isset($purl['port'])) {
                $nurl .= ':' . $purl['port'];
            }
            $nurl .= $purl['path'];
            $node->set_url($nurl);
            if (isset($purl['user']) && isset($purl['pass'])) {
                $node->set_auth($purl['user'], $purl['pass']);
            }
            $checksum = $newchecksum;
        }

        return $node;
    }

    // }}}
    // {{{ public methods

    /**
     * Register the text.
     *
     * @param   string  $url    The url of a node server.
     *                          Also includes the username and the password.
     * @param   string  $text       A text data.
     * @param   array   $attrs      Associated array of the attributes.
     *                              At least, requires `@uri' attribute.
     * @param   array   $keywords   A list of keywords. (optional)
     * @return  bool    True if success, else false.
     * @access  public
     * @static
     */
    function register($url, $text,$attrs, $keywords = null)
    {
        $node = &EstraierPure::get_node($url);
        if (!$node) {
            return false;
        }
        $doc = &new EstraierPure_Document;
        foreach (preg_split('/(?:\\r\\n|\\r|\\n)+/', trim($text)) as $line) {
            if (substr($line, 0, 1) == "\n") {
                $doc->add_hidden_text($line);
            } else {
                $doc->add_text($line);
            }
        }
        foreach ($attributes as $name => $value) {
            $doc->add_attr($name, $value);
        }
        if ($keywords) {
            $doc->set_keywords($keywords);
        }
        return $node->put_doc($doc);
    }

    /**
     * Update the registered document.
     *
     * @param   string  $url    The url of a node server.
     *                          Also includes the username and the password.
     * @param   int|string  $id     The ID number of a registered document
     *                              or the URI of a registered document.
     * @param   string  $text       An additional text data. (optional)
     * @param   array   $attrs      Associated array of the attributes. (optional)
     * @param   array   $keywords   A list of keywords. (optional)
     * @return  bool    True if success, else false.
     * @access  public
     * @static
     */
    function update($url, $id, $text = '', $attrs = null, $keywords = null)
    {
        $node = &EstraierPure::get_node($url);
        if (!$node) {
            return false;
        }
        if (is_int($id)) {
            $doc = $node->get_doc($id);
        } else if (is_string($id)) {
            $doc = $node->get_doc_by_uri($id);
        } else {
            trigger_error(sprintf(
                    'Argument#2 should be a kind of integer or string, %s given.',
                    gettype($id)), E_USER_WARNING);
            return false;
        }
        if (!$doc) {
            return false;
        }
        if (strlen($text)) {
            foreach (preg_split('/(?:\\r\\n|\\r|\\n)+/', trim($text)) as $line) {
                if (substr($line, 0, 1) == "\t") {
                    $doc->add_hidden_text($line);
                } else {
                    $doc->add_text($line);
                }
            }
        }
        if ($attributes) {
            foreach ($attributes as $name => $value) {
                $doc->add_attr($name, $value);
            }
        }
        if ($keywords) {
            $doc->set_keywords($keywords);
        }
        return $doc->edit_doc($doc);
    }

    /**
     * Replace the registered document.
     *
     * @param   string  $url    The url of a node server.
     *                          Also includes the username and the password.
     * @param   int|string  $id     The ID number of a registered document
     *                              or the URI of a registered document.
     * @param   string  $text       A text data.
     * @param   array   $attrs      Associated array of the attributes.
     *                              At least, requires `@uri' attribute.
     * @param   array   $keywords   A list of keywords. (optional)
     * @return  bool    True if success, else false.
     * @access  public
     * @static
     * @see EstraierPure::register()
     * @see EstraierPure::purge()
     */
    function replace($url, $id, $text, $attrs, $keywords = null)
    {
        $node = &EstraierPure::get_node($url);
        if (!$node) {
            return false;
        }
        if (is_int($id)) {
            $doc = $node->get_doc($id);
        } else if (is_string($id)) {
            $doc = $node->get_doc_by_uri($id);
        } else {
            trigger_error(sprintf(
                    'Argument#2 should be a kind of integer or string, %s given.',
                    gettype($id)), E_USER_WARNING);
            return false;
        }
        if ($doc && !EstraierPure::purge($url, $id)) {
            return false;
        }
        return EstraierPure::register($url, $text, $attributes, $keywords);
    }

    /**
     * Purge the registered document.
     *
     * @param   string  $url    The url of a node server.
     *                          Also includes the username and the password.
     * @param   int|string  $id     The ID number of a registered document
     *                              or the URI of a registered document.
     * @return  bool    True if success, else false.
     * @access  public
     * @static
     */
    function purge($url, $id)
    {
        $node = &EstraierPure::get_node($url);
        if (!$node) {
            return false;
        }
        if (is_int($id)) {
            return $node->out_doc($id);
        } else if (is_string($id)) {
            return $node->out_doc_by_uri($id);
        } else {
            trigger_error(sprintf(
                    'Argument#1 should be a kind of integer or string, %s given.',
                    gettype($id)), E_USER_WARNING);
            return false;
        }
    }

    /**
     * Search for documents corresponding a phrase.
     *
     * @param   string  $url    The url of a node server.
     *                          Also includes the username and the password.
     * @param   string  $phrase A search phrase.
     * @param   int     $limit  The maximum number of retrieval.
     *                          By default, the number of retrieval is not limited.
     * @param   int     $offset The number of documents to be skipped.
     *                          By default, it is 0.
     * @return  object  EstraierPure_NodeResult
     *                  A node result object.
     *                  On error, returns `null'.
     * @access  public
     * @static
     */
    function search($url, $phrase, $limit = -1, $offset = 0)
    {
        $node = &EstraierPure::get_node($url);
        if (!$node) {
            return null;
        }
        $cond = &new EstraierPure_Condition;
        $cond->set_phrase($phrase);
        $cond->set_max($limit);
        $cond->set_skip($offset);
        $nres = &$node->search($cond, 0);
        return $nres;
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_Error

/**
 * Class for error handling.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.6.0
 * @static
 */
class EstraierPure_Error
{
    // {{{ public methods

    /**
     * Get an instance of PEAR_ErrorStack.
     *
     * @return  object  PEAR_ErrorStack
     * @access  public
     * @static
     */
    function &getStack()
    {
        static $stack = null;
        if (!$stack) {
            $stack = &PEAR_ErrorStack::singleton('EstraierPure');
        }
        return $stack;
    }

    /**
     * Push an error callback.
     *
     * @param   callback    $callback   Error callback function/method.
     * @return  void
     * @access  public
     * @static
     * @see PEAR_ErrorStack::pushCallback()
     */
    function pushCallback($callback)
    {
        $stack = &EstraierPure_Error::getStack();
        $stack->pushCallback($callback);
    }

    /**
     * Pop an error callback.
     *
     * @return  callback|false
     * @access  public
     * @static
     * @see PEAR_ErrorStack::popCallback()
     */
    function popCallback()
    {
        $stack = &EstraierPure_Error::getStack();
        return $stack->popCallback();
    }

    /**
     * Add an error to the stack.
     *
     * @param   int     $code       Error code.
     * @param   string  $message    Error message.
     * @param   string  $level      Error level.
     * @param   array   $params     Associated array of error parameters.
     * @param   array   $repackage  Associated array of repackaed
     *                              error/exception classes.
     * @param   array   $backtrace  Error backtrace.
     * @return  PEAR_Error|array|Exception
     * @access  public
     * @static
     * @see PEAR_ErrorStack::push()
     */
    function push($code, $message = false, $level = 'error',
                  $params = array(), $repackage = false, $backtrace = false)
    {
        if (!$backtrace) {
            $backtrace = debug_backtrace();
        }
        $stack = &EstraierPure_Error::getStack();
        return $stack->push($code, $level, $params, $message, $repackage, $backtrace);
    }

    /**
     * Pop an error off of the error stack.
     *
     * @return  array|false
     * @access  public
     * @static
     * @see PEAR_ErrorStack::pop()
     */
    function pop()
    {
        $stack = &EstraierPure_Error::getStack();
        return $stack->pop();
    }

    /**
     * Determine whether there are any errors on the stack.
     *
     * @param   string  $level  Level name.
     * @return  bool
     * @access  public
     * @static
     * @see PEAR_ErrorStack::hasErrors()
     */
    function hasErrors($level = false)
    {
        $stack = &EstraierPure_Error::getStack();
        return $stack->hasErrors($level);
    }

    /**
     * Retrieve all errors since last purge.
     *
     * @param   bool    $purge  Whether empty the error statck or not.
     * @param  string   $level  Level name.
     * @return  array
     * @access  public
     * @static
     * @see PEAR_ErrorStack::getErrors()
     */
    function getErrors($purge = false, $level = false)
    {
        $stack = &EstraierPure_Error::getStack();
        return $stack->getErrors($purge, $level);
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_Utility

/**
 * Class for utility.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.0.1
 * @static
 */
class EstraierPure_Utility
{
    // {{{ public methods

    /**
     * Check types of arguments.
     *
     * @param   array   $types  Pairs of the argument and the expected type.
     * @return  void
     * @access  public
     * @static
     * @ignore
     */
    function check_types()
    {
        $backtrace = debug_backtrace();
        $arguments = func_get_args();
        $i = 0;
        foreach ($arguments as $types) {
            $i++;
            $var = array_shift($types);
            $type = gettype($var);
            $is_error = false;
            if ($types[0] == 'object' && isset($types[1]) && class_exists($types[1])) {
                if (!is_object($var) || !is_a($var, $types[1])) {
                    $is_error = true;
                    $params = array(
                        'argnum' => $i,
                        'expected' => $types[1],
                        'given' => (is_object($var)) ? get_class($var) : $type,
                    );
                }
            } elseif (!in_array($type, $types)) {
                $is_error = true;
                $params = array(
                    'argnum' => $i,
                    'expected' => implode(' or ', $types),
                    'given' => $type,
                );
            }
            if ($is_error) {
                $message = vsprintf('Argument#%d should be a kind of %s, %s given.',
                                    array_values($params));
                EstraierPure_Error::push(ESTRAIERPURE_ERROR_INVALID_ARGUMENT, $message,
                                         'warning', $params, false, $backtrace);
                trigger_error($message, E_USER_WARNING);
            }
        }
    }

    /**
     * Perform an interaction of a URL.
     *
     * @param   string  $url        A URL.
     * @param   string  $pxhost     The host name of a proxy.
     *                              If it is `null', it is not used.
     * @param   int     $pxport     The port number of the proxy.
     * @param   int     $outsec     Timeout in seconds.
     *                              If it is negative, it is not used.
     * @param   array   $reqheads   An array of extension headers.
     *                              If it is `null', it is not used.
     * @param   string  $reqbody    The pointer of the entitiy body of request.
     *                              If it is `null', "GET" method is used.
     * @return  object  EstraierPure_Response
     *                  An object into which headers and the entity body
     *                  of response are stored. On error, returns false.
     * @access  public
     * @static
     * @ignore
     */
    function &shuttle_url($url, $pxhost = null, $pxport = null,
                          $outsec = -1, $reqheads = null, $reqbody = null)
    {
        if (is_null($reqheads)) {
            $reqheads = array();
        }

        // set request parameters
        $params = array('http'=>array());
        if (is_null($reqbody)) {
            $params['http']['method'] = 'GET';
        } else {
            $params['http']['method'] = 'POST';
            $params['http']['content'] = $reqbody;
            $reqheads['content-length'] = strlen($reqbody);
        }
        if (!is_null($pxhost)) {
            $params['http']['proxy'] = sprintf('tcp://%s:%d', $pxhost, $pxport);
        }
        $params['http']['header'] = '';
        $reqheads['user-agent'] = sprintf('EstraierPure/%s (PHP %s)',
                                          ESTRAIERPURE_VERSION, PHP_VERSION);
        foreach ($reqheads as $key => $value) {
            $params['http']['header'] .= sprintf("%s: %s\r\n", $key, $value);
        }
        $context = stream_context_create($params);

        // open a stream and send the request
        $fp = fopen($url, 'r', false, $context);
        if (!$fp) {
            EstraierPure_Error::push(ESTRAIERPURE_ERROR_CONNECTION_FAILED,
                                     sprintf('Cannot connect to %s.', $url));
            $err = false;
            return $err;
        }
        if ($outsec >= 0) {
            stream_set_timeout($fp, $outsec);
        }

        // get the response body
        $body = '';
        while (!feof($fp)) {
            $body .= fread($fp, 50000);
        }

        // parse the response headers
        $meta_data = stream_get_meta_data($fp);
        if (!empty($meta_data['timed_out'])) {
            fclose($fp);
            EstraierPure_Error::push(ESTRAIERPURE_ERROR_CONNECTION_TIMEDOUT,
                                     'Connection timed out.');
            $err = false;
            return $err;
        }
        if (strcasecmp($meta_data['wrapper_type'], 'cURL') == 0) {
            $raw_headers = $meta_data['wrapper_data']['headers'];
        } else {
            $raw_headers = $meta_data['wrapper_data'];
        }
        $http_status = array_shift($raw_headers);
        if (!preg_match('!^HTTP/(.+?) (\\d+) ?(.*)!', $http_status, $matches)) {
            fclose($fp);
            EstraierPure_Error::push(ESTRAIERPURE_ERROR_MALFORMED_RESPONSE,
                                     'Malformed response.');
            $err = false;
            return $err;
        }
        $code = (int)$matches[2];
        $headers = array();
        foreach ($raw_headers as $header) {
            list($name, $value) = explode(':', $header, 2);
            $headers[strtolower($name)] = ltrim($value);
        }

        // close the stream
        fclose($fp);

        $res = &new EstraierPure_Response($code, $headers, $body);
        return $res;
    }

    /**
     * Serialize a condition object into a query string.
     *
     * @param   object  $cond   EstraierPure_Condition
     *                          which is a condition object.
     * @param   int     $depth  Depth of meta search.
     * @param   int     $wwidth Whole width of a snippet.
     * @param   int     $hwidth Width of strings picked up from the beginning of the text.
     * @param   int     $awidth Width of strings picked up around each highlighted word.
     * @return  string  The serialized string.
     * @access  public
     * @static
     * @ignore
     */
    function cond_to_query(&$cond, $depth, $wwidth, $hwidth, $awidth)
    {
        $query = '';
        if ($phrase = $cond->phrase()) {
            $query .= '&phrase=' . urlencode($phrase);
        }
        if ($attrs = $cond->attrs()) {
            $i = 0;
            foreach ($attrs as $attr) {
                $query .= '&attr' . strval(++$i) . '=' . urlencode($attr);
            }
        }
        if (strlen($order = $cond->order()) > 0) {
            $query .= '&order=' . urlencode($order);
        }
        $query .= '&max=' . strval((($max = $cond->max()) >= 0) ? $max : 1 << 30);
        if (($options = $cond->options()) > 0) {
            $query .= '&options=' . strval($options);
        }
        $query .= '&auxiliary=' . strval($cond->auxiliary());
        if (strlen($distinct = $cond->distinct()) > 0) {
            $query .= '&distinct=' . urlencode($distinct);
        }
        if ($depth > 0) {
            $query .= '&depth=' . strval($depth);
        }
        $query .= '&wwidth=' . strval($wwidth);
        $query .= '&hwidth=' . strval($hwidth);
        $query .= '&awidth=' . strval($awidth);
        $query .= '&skip=' . strval($cond->skip());
        $query .= '&mask=' . strval($cond->mask());
        return substr($query, 1);
    }

    /**
     * Sanitize an attribute name, an attribute value or a hidden sentence.
     *
     * @param   string  $str  A non-sanitized string.
     * @return  string  The sanitized string.
     * @access  public
     * @static
     * @deprecated  Method deprecated in Release 0.6.0
     * @ignore
     */
    function sanitize($str)
    {
        return trim(preg_replace('/[ \\t\\r\\n\\x0B\\f]+/', ' ', $str), ' ');
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_Document

/**
 * Abstraction of document.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.0.1
 */
class EstraierPure_Document
{
    // {{{ properties

    /**
     * The ID number
     *
     * @var int
     * @access  private
     */
    var $id;

    /**
     * Attributes
     *
     * @var array
     * @access  private
     */
    var $attrs;

    /**
     * Sentences of text
     *
     * @var array
     * @access  private
     */
    var $dtexts;

    /**
     * Hidden sentences of text
     *
     * @var array
     * @access  private
     */
    var $htexts;

    /**
     * Keywords
     *
     * @var array
     * @access  private
     */
    var $kwords;

    /**
     * Substiture score
     *
     * @var int
     * @access  private
     */
    var $score;

    // }}}
    // {{{ constructor

    /**
     * Create a document object.
     *
     * @param   string  $draft  A string of draft data.
     * @access  public
     */
    function EstraierPure_Document($draft = '')
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($draft, 'string')
        );
        $this->id = -1;
        $this->attrs = array();
        $this->dtexts = array();
        $this->htexts = array();
        $this->kwords = null;
        $this->score = -1;
        if (strlen($draft)) {
            $lines = explode("\n", $draft);
            $num = 0;
            $len = count($lines);
            while ($num < $len) {
                $line = $lines[$num];
                $num++;
                if (strlen($line) == 0) {
                    break;
                }
                if (substr($line, 0, 1) == '%') {
                    if (substr($line, 8) == "%VECTOR\t") {
                        $fields = explode("\t", $line);
                        $i = 1;
                        $flen = count($fields) - 1;
                        while ($i < $flen) {
                            $this->kwords[$fields[$i]] = $fields[$i+1];
                            $i += 2;
                        }
                    } elseif (substr($line, 7) == "%SCORE\t") {
                        $fields = explode("\t", $line);
                        $this->score = (int)$fields[1];
                    }
                    continue;
                }
                $line = EstraierPure_Utility::sanitize($line);
                if (strpos($line, '=')) {
                    list($key, $value) = explode('=', $line, 2);
                    $this->attrs[$key] = $value;
                }
            }
            while ($num < $len) {
                $line = $lines[$num];
                $num++;
                if (strlen($line) == 0) {
                    continue;
                }
                if (substr($line, 0, 1) == "\t") {
                    if (strlen($line) > 1) {
                        $this->_htexts[] = substr($line, 1);
                    }
                } else {
                    $this->dtexts[] = $line;
                }
            }
        }
    }

    // }}}
    // {{{ setter methods

    /**
     * Add an attribute.
     *
     * @param   string  $name   The name of an attribute.
     * @param   string  $value  The value of the attribute.
     *                          If it is `null', the attribute is removed.
     * @return  void
     * @access  public
     */
    function add_attr($name, $value = null)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($name, 'string'), array($value, 'string', 'NULL')
        );
        $name = EstraierPure_Utility::sanitize($name);
        $value = EstraierPure_Utility::sanitize($value);
        if (!is_null($value)) {
            $this->attrs[$name] = $value;
        } else {
            unset($this->attrs[$name]);
        }
    }

    /**
     * Add a sentence of text.
     *
     * @param   string  $text   A sentence of text.
     * @return  void
     * @access  public
     */
    function add_text($text)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($text, 'string')
        );
        $text = EstraierPure_Utility::sanitize($text);
        if (strlen($text)) {
            $this->dtexts[] = $text;
        }
    }

    /**
     * Add a hidden sentence.
     *
     * @param   string  $text   A hidden sentence.
     * @return  void
     * @access  public
     */
    function add_hidden_text($text)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($text, 'string')
        );
        $text = EstraierPure_Utility::sanitize($text);
        if (strlen($text)) {
            $this->htexts[] = $text;
        }
    }

    /**
     * Attach keywords.
     *
     * @param   array   $kwords A list of keywords.
     *                          Keys of the map should be keywords of the document
     *                          and values should be their scores in decimal string.
     * @return  void
     * @access  public
     */
    function set_keywords($kwords)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($kwords, 'array')
        );
        $this->kwords = $kwords;
    }

    /**
     * Set the substitute score.
     *
     * @param   int     $score  The substitute score.
     *                          It it is negative, the substitute score setting is nullified.
     * @return  void
     * @access  public
     */
    function set_score($score)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($score, 'integer')
        );
        $this->score = $score;
    }

    // }}}
    // {{{ getter methods

    /**
     * Get the ID number.
     *
     * @return  int     The ID number of the document object.
     *                  If the object has never beenregistered, returns -1.
     * @access  public
     */
    function id()
    {
        return $this->id;
    }

    /**
     * Get an array of attribute names of a document object.
     *
     * @return  array   Attribute names.
     * @access  public
     */
    function attr_names()
    {
        $names = array_keys($this->attrs);
        sort($names);
        return $names;
    }

    /**
     * Get the value of an attribute.
     *
     * @param   string  $name   The name of an attribute.
     * @return  string  The value of the attribute. If it does not exist, returns `null'.
     * @access  public
     */
    function attr($name)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($name, 'string')
        );
        return (isset($this->attrs[$name])) ? $this->attrs[$name] : null;
    }

    /**
     * Get an array of sentences of the text.
     *
     * @return  array   Sentences of the text.
     * @access  public
     */
    function texts()
    {
        return $this->dtexts;
    }

    /**
     * Concatenate sentences of the text of a document object.
     *
     * @return  string  Concatenated sentences.
     * @access  public
     */
    function cat_texts()
    {
        return implode(' ', $this->dtexts);
    }

    /**
     * Dump draft data of a document object.
     *
     * @return  string  The draft data.
     * @access  public
     */
    function dump_draft()
    {
        $buf = '';
        foreach ($this->attr_names() as $name) {
            $buf .= sprintf("%s=%s\n", $name, $this->attrs[$name]);
        }
        if ($this->kwords) {
            $buf .= '%VECTOR';
            foreach ($this->kwords as $key => $value) {
                $buf .= sprintf("\t%s\t%s", $key, $value);
            }
            $buf .= "\n";
        }
        $buf .= "\n";
        if ($this->dtexts) {
            $buf .= implode("\n", $this->dtexts) . "\n";
        }
        if ($this->htexts) {
            $buf .= "\t" . implode("\n\t", $this->htexts) . "\n";
        }
        return $buf;
    }

    /**
     * Get attached keywords.
     *
     * @return  array   A list of keywords and their scores in decimal string.
     *                  If no keyword is attached, `null' is returned.
     * @access  public
     */
    function keywords()
    {
        return $this->kwords;
    }

    /**
     * Get the substitute score.
     *
     * @return  int     The substitute score or -1 if it is not set.
     * @access  public
     */
    function score()
    {
        return $this->score;
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_Condition

// {{{ constants

/**
 * option: check every N-gram key
 */
define('ESTRAIERPURE_CONDITION_SURE', 1 << 0);

/**
 * option: check N-gram keys skipping by one
 */
define('ESTRAIERPURE_CONDITION_USUAL', 1 << 1);

/**
 * option: check N-gram keys skipping by two
 */
define('ESTRAIERPURE_CONDITION_FAST', 1 << 2);

/**
 * option: check N-gram keys skipping by three
 */
define('ESTRAIERPURE_CONDITION_AGITO', 1 << 3);

/**
 * option: without TF-IDF tuning
 */
define('ESTRAIERPURE_CONDITION_NOIDF', 1 << 4);

/**
 * option: with the simplified phrase
 */
define('ESTRAIERPURE_CONDITION_SIMPLE', 1 << 10);

/**
 * option: with the rough phrase
 */
define('ESTRAIERPURE_CONDITION_ROUGH', 1 << 11);

/**
 * option: with the union phrase
 */
define('ESTRAIERPURE_CONDITION_UNION', 1 << 15);

/**
 * option: with the intersection phrase
 */
define('ESTRAIERPURE_CONDITION_ISECT', 1 << 16);

// }}}

/**
 * Abstraction of search condition.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.0.1
 */
class EstraierPure_Condition
{
    // {{{ properties

    /**
     * The search phrase
     *
     * @var string
     * @access  private
     */
    var $phrase;

    /**
     * The order of a condition object
     *
     * @var string
     * @access  private
     */
    var $order;

    /**
     * The maximum number of retrieval
     *
     * @var int
     * @access  private
     */
    var $max;

    /**
     * The number of documents to be skipped.
     *
     * @var int
     * @access  private
     */
    var $skip;

    /**
     * Options of retrieval
     *
     * @var int
     * @access  private
     */
    var $options;

    /**
     * Permission to adopt result of the auxiliary index
     *
     * @var int
     * @access  private
     */
    var $auxiliary;

    /**
     * The name of the distinct attribute
     *
     * @var string
     * @access  private
     */
    var $distinct;

    /**
     * The mask of targets of meta search
     *
     * @var int
     * @access  private
     */
    var $mask;

    // }}}
    // {{{ constructor

    /**
     * Create a search condition object.
     *
     * @access  public
     */
    function EstraierPure_Condition()
    {
        $this->phrase = null;
        $this->attrs = array();
        $this->order = null;
        $this->max = -1;
        $this->skip = 0;
        $this->options = 0;
        $this->auxiliary = 32;
        $this->distinct = null;
        $this->mask = 0;
    }

    // }}}
    // {{{ setter methods

    /**
     * Set the search phrase.
     *
     * @param   string  $phrase     A search phrase.
     * @return  void
     * @access  public
     */
    function set_phrase($phrase)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($phrase, 'string')
        );
        $this->phrase = EstraierPure_Utility::sanitize($phrase);
    }

    /**
     * Add an expression for an attribute.
     *
     * @param   string  $expr   A search expression.
     * @return  void
     * @access  public
     */
    function add_attr($expr)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($expr, 'string')
        );
        $this->attrs[] = EstraierPure_Utility::sanitize($expr);
    }

    /**
     * Set the order of a condition object.
     *
     * @param   string  $order  An expression for the order.
     *                          By default, the order is by score descending.
     * @return  void
     * @access  public
     */
    function set_order($order)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($order, 'string')
        );
        $this->order = EstraierPure_Utility::sanitize($order);
    }

    /**
     * Set the maximum number of retrieval.
     *
     * @param   int     $max    The maximum number of retrieval.
     *                          By default, the number of retrieval is not limited.
     * @return  void
     * @access  public
     */
    function set_max($max)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($max, 'integer')
        );
        if ($max >= 0) {
            $this->max = $max;
        }
    }

    /**
     * Set the number of documents to be skipped.
     *
     * @param   int     $skip   The number of documents to be skipped.
     *                          By default, it is 0.
     * @return  void
     * @access  public
     */
    function set_skip($skip)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($skip, 'integer')
        );
        if ($skip >= 0) {
            $this->skip = $skip;
        }
    }

    /**
     * Set options of retrieval.
     *
     * @param   int     $options    Options:
     * - `ESTRAIERPURE_CONDITION_SURE' specifies that it checks every N-gram key.
     * - `ESTRAIERPURE_CONDITION_USUAL', which is the default,
     *      specifies that it checks N-gram keys with skipping one key.
     * - `ESTRAIERPURE_CONDITION_FAST' skips two keys.
     * - `ESTRAIERPURE_CONDITION_AGITO' skips three keys.
     * - `ESTRAIERPURE_CONDITION_NOIDF' specifies not to perform TF-IDF tuning.
     * - `ESTRAIERPURE_CONDITION_SIMPLE' specifies to use simplified phrase.
     * - `ESTRAIERPURE_CONDITION_ROUGH' specifies to use rough phrase.
     * - `ESTRAIERPURE_CONDITION_UNION' specifies to use union phrase.
     * - `ESTRAIERPURE_CONDITION_ISECT' specifies to use intersection phrase.
     *  Each option can be specified at the same time by bitwise or.
     *  If keys are skipped, though search speed is improved, the relevance ratio grows less.
     * @return  void
     * @access  public
     */
    function set_options($options)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($options, 'integer')
        );
        $this->options |= $options;
    }

    /**
     * Set permission to adopt result of the auxiliary index.
     *
     * @param   int     $min    The minimum hits to adopt result of the auxiliary index.
     *                          If it is not more than 0, the auxiliary index is not used.
     *                          By default, it is 32.
     * @return  void
     * @access  public
     */
    function set_auxiliary($min)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($min, 'integer')
        );
        $this->auxiliary = $min;
    }

    /**
     * Set the attribute distinction filter.
     *
     * @param   string  $name   The name of an attribute to be distinct.
     * @return  void
     * @access  public
     */
    function set_distinct($name)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($name, 'string')
        );
        $this->distinct = EstraierPure_Utility::sanitize($name);
    }

    /**
     * Set the mask of targets of meta search.
     *
     * @param   int     $mask   A masking number.
     *                          1 means the first target, 2 means the second target,
     *                          4 means the third target, and power values of 2 and
     *                          their summation compose the mask.
     * @return  void
     * @access  public
     */
    function set_mask($mask)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($mask, 'integer')
        );
        $this->mask = $mask;
    }

    // }}}
    // {{{ getter methods

    /**
     * Get the search phrase.
     *
     * @return  string  The search phrase.
     * @access  public
     */
    function phrase()
    {
        return $this->phrase;
    }

    /**
     * Get expressions for attributes.
     *
     * @return  array   Expressions for attributes.
     * @access  public
     */
    function attrs()
    {
        return $this->attrs;
    }

    /**
     * Get the order expression.
     *
     * @return  string  The order expression.
     * @access  public
     */
    function order()
    {
        return $this->order;
    }

    /**
     * Get the maximum number of retrieval.
     *
     * @return  int     The maximum number of retrieval.
     * @access  public
     */
    function max()
    {
        return $this->max;
    }

    /**
     * Get the number of documents to be skipped.
     *
     * @return  int     The number of documents to be skipped.
     * @access  public
     */
    function skip()
    {
        return $this->skip;
    }

    /**
     * Get options of retrieval.
     *
     * @return  int     Options by bitwise or.
     * @access  public
     */
    function options()
    {
        return $this->options;
    }

    /**
     * Get permission to adopt result of the auxiliary index.
     *
     * @return  int     Permission to adopt result of the auxiliary index.
     * @access  public
     */
    function auxiliary()
    {
        return $this->auxiliary;
    }

    /**
     * Get the attribute distinction filter.
     *
     * @return  string  The name of the distinct attribute.
     * @access  public
     */
    function distinct()
    {
        return $this->distinct;
    }

    /**
     * Get the mask of targets of meta search.
     *
     * @return  int     The mask of targets of meta search.
     * @access  public
     */
    function mask()
    {
        return $this->mask;
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_ResultDocument

/**
 * Abstraction document in result set.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.0.1
 */
class EstraierPure_ResultDocument
{
    // {{{ properties

    /**
     * The URI of the result document object
     *
     * @var string
     * @access  private
     */
    var $uri;

    /**
     * A list of attribute names
     *
     * @var array
     * @access  private
     */
    var $attrs;

    /**
     * Snippet of a result document object
     *
     * @var string
     * @access  private
     */
    var $snippet;

    /**
     * The keyword vector
     *
     * @var string
     * @access  private
     */
    var $keywords;

    // }}}
    // {{{ constructor

    /**
     * Create a result document object.
     *
     * @param   string  $uri        The URI of the result document object.
     * @param   array   $attrs      A list of attribute names.
     * @param   string  $snippet    The snippet of a result document object
     * @param   string  $keywords   Keywords of the result document object.
     * @access  public
     */
    function EstraierPure_ResultDocument($uri, $attrs, $snippet, $keywords)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($uri, 'string'), array($attrs, 'array'),
            array($snippet, 'string'), array($keywords, 'string')
        );
        $this->uri = $uri;
        $this->attrs = $attrs;
        $this->snippet = $snippet;
        $this->keywords = $keywords;
    }

    // }}}
    // {{{ getter methods

    /**
     * Get the URI.
     *
     * @return  string  The URI of the result document object.
     * @access  public
     */
    function uri()
    {
        return $this->uri;
    }

    /**
     * Get a list of attribute names.
     *
     * @return  array   A list of attribute names.
     * @access  public
     */
    function attr_names()
    {
        $names = array_keys($this->attrs);
        sort($names);
        return $names;
    }

    /**
     * Get the value of an attribute.
     *
     * @param   string  $name   The name of an attribute.
     * @return  string  The value of the attribute. If it does not exist, returns `null'.
     * @access  public
     */
    function attr($name)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($name, 'string')
        );
        return (isset($this->attrs[$name])) ? $this->attrs[$name] : null;
    }

    /**
     * Get the snippet of a result document object.
     *
     * @return  string  The snippet of the result document object.
     *                  There are tab separated values.
     *                  Each line is a string to be shown.
     *                  Though most lines have only one field,
     *                  some lines have two fields.
     *                  If the second field exists, the first field isto be shown with
     *                  highlighted, and the second field means its normalized form.
     * @access  public
     */
    function snippet()
    {
        return $this->snippet;
    }

    /**
     * Get keywords of a result document object.
     *
     * @return  string  Serialized keywords of the result document object.
     *                  There are tab separated values.
     *                  Keywords and their scores come alternately.
     * @access  public
     */
    function keywords()
    {
        return $this->keywords;
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_NodeResult

/**
 * Abstraction of result set from node.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.0.1
 */
class EstraierPure_NodeResult
{
    // {{{ properties

    /**
     * Documents
     *
     * @var array
     * @access  private
     */
    var $docs;

    /**
     * Hint informations
     *
     * @var array
     * @access  private
     */
    var $hints;

    // }}}
    // {{{ constructor

    /**
     * Create a node result object.
     *
     * @param   array   $docs   Documents.
     * @param   array   $hints  Hint informations.
     * @access  public
     */
    function EstraierPure_NodeResult($docs, $hints)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($docs, 'array'), array($hints, 'array')
        );
        $this->docs = $docs;
        $this->hints = $hints;
    }

    // }}}
    // {{{ getter methods

    /**
     * Get the number of documents.
     *
     * @return  int     The number of documents.
     * @access  public
     */
    function doc_num()
    {
        return count($this->docs);
    }

    /**
     * Get a document object.
     *
     * @param   int     $index  The index of a document.
     * @return  object  EstraierPure_ResultDocument
     *                  A result document object.
     *                  If the index is out of bounds, returns `null'.
     * @access  public
     */
    function &get_doc($index)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($index, 'integer')
        );
        if (isset($this->docs[$index])) {
            return $this->docs[$index];
        } else {
            $null = null;
            return $null;
        }
    }

    /**
     * Get the value of hint information.
     *
     * @param   string  $key    The key of a hint.
     *                          "VERSION", "NODE", "HIT", "HINT#n", "DOCNUM",
     *                          "WORDNUM", "TIME", "TIME#n", "LINK#n", and "VIEW"
     *                          are provided for keys.
     * @return  string  The hint. If the key does not exist, returns `null'.
     * @access  public
     */
    function hint($key)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($key, 'string')
        );
        return (isset($this->hints[$key])) ? $this->hints[$key] : null;
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_Node

// {{{ constants

/**
 * mode: delete the account
 */
define('ESTRAIERPURE_NODE_USER_DELETE', 0);

/**
 * mode: set the account as an administrator
 */
define('ESTRAIERPURE_NODE_ USER_ADMIN', 1);

/**
 * mode: set the account as a guest
 */
define('ESTRAIERPURE_NODE_USER_GUEST', 2);

// }}}

/**
 * Abstraction of connection to P2P node.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.0.1
 * @uses        PEAR
 */
class EstraierPure_Node
{
    // {{{ properties

    /**
     * The URL of a node server
     *
     * @var string
     * @access  private
     */
    var $url;

    /**
     * The host name of a proxy server
     *
     * @var string
     * @access  private
     */
    var $pxhost;

    /**
     * The port number of the proxy server
     *
     * @var int
     * @access  private
     */
    var $pxport;

    /**
     * Timeout of the connection in seconds
     *
     * @var int
     * @access  private
     */
    var $timeout;

    /**
     * The authentication information
     *
     * @var string
     * @access  private
     */
    var $auth;

    /**
     * The name of the node
     *
     * @var string
     * @access  private
     */
    var $name;

    /**
     * The label of the node
     *
     * @var string
     * @access  private
     */
    var $label;

    /**
     * The number of documents
     *
     * @var int
     * @access  private
     */
    var $dnum;

    /**
     * The number of unique words
     *
     * @var int
     * @access  private
     */
    var $wnum;

    /**
     * The size of the datbase
     *
     * @var float
     * @access  private
     */
    var $size;

    /**
     * Names of administrators
     *
     * @var array
     * @access  private
     */
    var $admins;

    /**
     * Names of users
     *
     * @var array
     * @access  private
     */
    var $users;

    /**
     * Expressions of links.
     *
     * @var array
     * @access  private
     */
    var $links;

    /**
     * Whole width of a snippet
     *
     * @var int
     * @access  private
     */
    var $wwidth;

    /**
     * Width of strings picked up from the beginning of the text
     *
     * @var int
     * @access  private
     */
    var $hwidth;

    /**
     * Width of strings picked up around each highlighted word
     *
     * @var int
     * @access  private
     */
    var $awidth;

    /**
     * The status code of the response
     *
     * @var int
     * @access  private
     */
    var $status;

    // }}}
    // {{{ constructor

    /**
     * Create a node connection object.
     *
     * @access  public
     */
    function EstraierPure_Node()
    {
        $this->url = null;
        $this->pxhost = null;
        $this->pxport = -1;
        $this->timeout = -1;
        $this->auth = null;
        $this->name = null;
        $this->label = null;
        $this->dnum = -1;
        $this->wnum = -1;
        $this->size = -1.0;
        $this->admins = null;
        $this->users = null;
        $this->links = null;
        $this->wwidth = 480;
        $this->hwidth = 96;
        $this->awidth = 96;
        $this->status = -1;
    }

    // }}}
    // {{{ setter methods

    /**
     * Set the URL of a node server.
     *
     * @param   string  $url    The URL of a node.
     * @return  void
     * @access  public
     */
    function set_url($url)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($url, 'string')
        );
        $this->url = $url;
    }

    /**
     * Set the URL of a node server.
     *
     * @param   string  $host   The host name of a proxy server.
     * @param   int     $port   The port number of the proxy server.
     * @return  void
     * @access  public
     */
    function set_proxy($host, $port)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($host, 'string'), array($port, 'integer')
        );
        $this->pxhost = $host;
        $this->pxport = $port;
    }

    /**
     * Set timeout of a connection.
     *
     * @param   int     $sec    Timeout of the connection in seconds.
     * @return  void
     * @access  public
     */
    function set_timeout($sec)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($sec, 'integer')
        );
        $this->timeout = $sec;
    }

    /**
     * Set the authentication information.
     *
     * @param   string  $name       The name of authentication.
     * @param   string  $password   The password of the authentication.
     * @return  void
     * @access  public
     */
    function set_auth($name, $password)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($name, 'string'), array($password, 'string')
        );
        $this->auth = $name . ':' . $password;
    }

    /**
     * Set width of snippet in the result.
     *
     * @param   int     $wwidth  Whole width of a snippet.
     *                           By default, it is 480.
     *                           If it is 0, no snippet is sent.
     *                           If it is negative, whole body text is sent
     *                           instead of snippet.
     * @param   int     $hwidth  Width of strings picked up from the beginning of the text.
     *                           By default, it is 96.
     *                           If it is negative 0, the current setting is not changed.
     * @param   int     $awidth  Width of strings picked up around each highlighted word.
     *                           By default, it is 96.
     *                           If it is negative, the current setting is not changed.
     * @return  void
     * @access  public
     */
    function set_snippet_width($wwidth, $hwidth, $awidth)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($wwidth, 'integer'), array($hwidth, 'integer'),
            array($awidth, 'integer')
        );
        $this->wwidth = $wwidth;
        if ($hwidth >= 0) {
            $this->hwidth = $hwidth;
        }
        if ($awidth >= 0) {
            $this->awidth = $awidth;
        }
    }

    // }}}
    // {{{ getter methods

    /**
     * Get the name.
     *
     * @return  string  The name.
     *                  On error, returns `null'.
     * @access  public
     */
    function name()
    {
        if (is_null($this->name)) {
            $this->set_info();
        }
        return $this->name;
    }

    /**
     * Get the label.
     *
     * @return  string  The label.
     *                  On error, returns `null'.
     * @access  public
     */
    function label()
    {
        if (is_null($this->label)) {
            $this->set_info();
        }
        return $this->label;
    }

    /**
     * Get the number of documents.
     *
     * @return  int     The number of documents.
     *                  On error, returns -1.
     * @access  public
     */
    function doc_num()
    {
        if ($this->dnum < 0) {
            $this->set_info();
        }
        return $this->dnum;
    }

    /**
     * Get the number of unique words.
     *
     * @return  int     The number of unique words.
     *                  On error, returns -1.
     * @access  public
     */
    function word_num()
    {
        if ($this->wnum < 0) {
            $this->set_info();
        }
        return $this->wnum;
    }

    /**
     * Get the size of the datbase.
     *
     * @return  float   The size of the datbase.
     *                  On error, returns -1.0.
     * @access  public
     */
    function size()
    {
        if ($this->size < 0.0) {
            $this->set_info();
        }
        return $this->size;
    }

    /**
     * Get an array of names of administrators.
     *
     * @return  array   Names of administrators.
     *                  On error, returns `null'.
     * @access  public
     */
    function admins()
    {
        if (is_null($this->admins)) {
            $this->set_info();
        }
        return $this->admins;
    }

    /**
     * Get an array of names of users.
     *
     * @return  array   Names of users.
     *                  On error, returns `null'.
     * @access  public
     */
    function users()
    {
        if (is_null($this->users)) {
            $this->set_info();
        }
        return $this->users;
    }

    /**
     * Get an array of expressions of links.
     *
     * @return  array   Expressions of links.
     *                  Each element is a TSV string and has three fields of
     *                  the URL,the label, and the score.
     *                  On error, returns `null'.
     * @access  public
     */
    function links()
    {
        if (is_null($this->links)) {
            $this->set_info();
        }
        return $this->links;
    }

    /**
     * Get the status code of the last request.
     *
     * @return  int     The status code of the last request.
     *                  -1 means failure of connection.
     * @access  public
     */
    function status()
    {
        return $this->status;
    }

    // }}}
    // {{{ document manipulation methods

    /**
     * Add a document.
     *
     * @param   object  $doc    EstraierPure_Document
     *                          which is a document object.
     *                          The document object should have the URI attribute.
     * @return  bool    True if success, else false.
     * @access  public
     */
    function put_doc(&$doc)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($doc, 'object', 'EstraierPure_Document')
        );
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/put_doc';
        $reqheads = array('content-type' => 'text/x-estraier-draft');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = $doc->dump_draft();
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    /**
     * Remove a document.
     *
     * @param   int     $id     The ID number of a registered document.
     * @return  bool    True if success, else false.
     * @access  public
     */
    function out_doc($id)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($id, 'integer')
        );
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/out_doc';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'id=' . $id;
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    /**
     * Remove a document specified by URI.
     *
     * @param   string  $uri    The URI of a registered document.
     * @return  bool    True if success, else false.
     * @access  public
     */
    function out_doc_by_uri($uri)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($uri, 'string')
        );
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/out_doc';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'uri=' . urlencode($uri);
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    /**
     * Edit attributes of a document.
     *
     * @param   object  $doc    EstraierPure_Document
     *                          which is a document object.
     * @return  bool    True if success, else false.
     * @access  public
     */
    function edit_doc(&$doc)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($doc, 'object', 'EstraierPure_Document')
        );
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/edit_doc';
        $reqheads = array('content-type' => 'text/x-estraier-draft');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = $doc->dump_draft();
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    /**
     * Retrieve a document.
     *
     * @param   int     $id     The ID number of a registered document.
     * @return  object  EstraierPure_Document
     *                  A document object.
     *                  On error, returns `null'.
     * @access  public
     */
    function &get_doc($id)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($id, 'integer')
        );
        $this->status = -1;
        if (!$this->url) {
            $null = null;
            return $null;
        }
        $turl = $this->url . '/get_doc';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'id=' . $id;
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return null;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            $null = null;
            return $null;
        }
        $doc = &new EstraierPure_Document($res->body());
        return $doc;
    }

    /**
     * Remove a document specified by URI.
     *
     * @param   string  $uri    The URI of a registered document.
     * @return  object  EstraierPure_Document
     *                  A document object.
     *                  On error, returns `null'.
     * @access  public
     */
    function &get_doc_by_uri($uri)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($uri, 'string')
        );
        $this->status = -1;
        if (!$this->url) {
            $null = null;
            return $null;
        }
        $turl = $this->url . '/get_doc';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'uri=' . urlencode($uri);
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return null;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            $null = null;
            return $null;
        }
        $doc = &new EstraierPure_Document($res->body());
        return $doc;
    }

    /**
     * Retrieve the value of an attribute of a document.
     *
     * @param   int     $id     The ID number of a registered document.
     * @param   string  $name   The name of an attribute.
     * @return  string  The value of the attribute. If it does not exist, returns `null'.
     * @access  public
     */
    function get_doc_attr($id, $name)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($id, 'integer'), array($name, 'string')
        );
        $this->status = -1;
        if (!$this->url) {
            return null;
        }
        $turl = $this->url . '/get_doc_attr';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'id=' . $id . '&attr=' . urlencode($name);
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return null;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return null;
        }
        return rtrim($res->body(), "\n");
    }

    /**
     * Retrieve the value of an attribute of a document specified by URI.
     *
     * @param   string  $uri    The URI of a registered document.
     * @param   string  $name   The name of an attribute.
     * @return  string  The value of the attribute. If it does not exist, returns `null'.
     * @access  public
     */
    function get_doc_attr_by_uri($uri, $name)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($uri, 'string'), array($name, 'string')
        );
        $this->status = -1;
        if (!$this->url) {
            return null;
        }
        $turl = $this->url . '/get_doc_attr';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'uri=' . urlencode($uri) . '&attr=' . urlencode($name);
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return null;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return null;
        }
        return rtrim($res->body(), "\n");
    }

    /**
     * Extract keywords of a document.
     *
     * @param   int     $id     The ID number of a registered document.
     * @return  array   Pairs of keywords and their scores in decimal string.
     *                  On error, returns `null'.
     * @access  public
     */
    function etch_doc($id)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($id, 'integer')
        );
        $this->status = -1;
        if (!$this->url) {
            return null;
        }
        $turl = $this->url . '/etch_doc';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'id=' . $id;
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return null;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return null;
        }
        $kwords = array();
        $lines = explode("\n", $res->body());
        foreach ($lines as $line) {
            if (strpos($line, "\t")) {
                $pair = explode("\t", $line);
                $kwords[$pair[0]] = $pair[1];
            }
        }
        return $kwords;
    }

    /**
     * Extract keywords of a document specified by URI.
     *
     * @param   string  $uri    The URI of a registered document.
     * @return  array   Pairs of keywords and their scores in decimal string.
     *                  On error, returns `null'.
     * @access  public
     */
    function etch_doc_by_uri($uri)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($uri, 'string')
        );
        $this->status = -1;
        if (!$this->url) {
            return null;
        }
        $turl = $this->url . '/etch_doc';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'uri=' . urlencode($uri);
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return null;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return null;
        }
        $kwords = array();
        $lines = explode("\n", $res->body());
        foreach ($lines as $line) {
            if (strpos($line, "\t")) {
                $pair = explode("\t", $line);
                $kwords[$pair[0]] = $pair[1];
            }
        }
        return $kwords;
    }

    // }}}
    // {{{ node management methods

    /**
     * Manage a user account of a node.
     *
     * @param   string  $name   The name of a user.
     * @param   int     $mode   The operation mode.
     * - `ESTRAIERPURE_NODE_USER_DELETE' means to delete the account.
     * - `ESTRAIERPURE_NODE_USER_ADMIN' means to set the account as an administrator.
     * - `ESTRAIERPURE_NODE_USER_GUEST' means to set the account as a guest.
     * @return  bool    True if success, else false.
     * @access  public
     */
    function set_user($name, $mode)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($name, 'string'), array($mode, 'integer')
        );
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/_set_user';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'name=' . urlencode($name) . '&mode=' . $mode;
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    /**
     * Manage a link of a node.
     *
     * @param   string  $url    The URL of the target node of a link.
     * @param   string  $label  The label of the link.
     * @param   int     $credit  The credit of the link.
     *                           If it is negative, the link is removed.
     * @return  bool    True if success, else false.
     * @access  public
     */
    function set_link($url, $label, $credit)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($url, 'string'), array($label, 'string'),
            array($credit, 'integer')
        );
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/_set_link';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'url=' . urlencode($url) . '&label=' . $label;
        if ($credit >= 0) {
            $reqbody .= '&credit=' . $credit;
        }
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    // }}}
    // {{{ database management methods

    /**
     * Synchronize updating contents of the database.
     *
     * @return  bool    True if success, else false.
     * @access  public
     */
    function sync()
    {
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/sync';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    /**
     * Optimize the database.
     *
     * @return  bool    True if success, else false.
     * @access  public
     */
    function optimize()
    {
        $this->status = -1;
        if (!$this->url) {
            return false;
        }
        $turl = $this->url . '/optimize';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads);
        if (!$res) {
            return false;
        }
        $this->status = $res->status();
        return $res->is_success();
    }

    // }}}
    // {{{ other public methods

    /**
     * Get the ID of a document specified by URI.
     *
     * @param   string  $uri    The URI of a registered document.
     * @return  int     The ID of the document.
     *                  On error, returns -1.
     * @access  public
     */
    function uri_to_id($uri)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($uri, 'string')
        );
        $this->status = -1;
        if (!$this->url) {
            return -1;
        }
        $turl = $this->url . '/uri_to_id';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = 'url=' . urlencode($url) . '&label=' . $label;
        if ($credit >= 0) {
            $reqbody .= '&credit=' . $credit;
        }
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return -1;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return -1;
        }
        return intval(rtrim($res->body(), "\n"));
    }

    /**
     * Get the usage ratio of the cache.
     *
     * @return  float   The usage ratio of the cache.
     *                  On error, -1.0 is returned.
     * @access  public
     */
    function cache_usage()
    {
        $this->status = -1;
        if (!$this->url) {
            return -1.0;
        }
        $turl = $this->url . '/cacheusage';
        $reqheads = array();
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads);
        if (!$res) {
            return -1.0;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return -1.0;
        }
        return floatval(trim($res->body()));
    }

    /**
     * Search for documents corresponding a condition.
     *
     * @param   object  $cond   EstraierPure_Condition
     *                          which is a condition object.
     * @param   int     $depth  The depth of meta search.
     * @return  object  EstraierPure_NodeResult
     *                  A node result object.
     *                  On error, returns `null'.
     * @access  public
     */
    function &search(&$cond, $depth)
    {
        ESTRAIERPURE_DEBUG && EstraierPure_Utility::check_types(
            array($cond, 'object', 'EstraierPure_Condition'), array($depth, 'integer')
        );
        $this->status = -1;
        if (!$this->url) {
            $null = null;
            return $null;
        }
        $turl = $this->url . '/search';
        $reqheads = array('content-type' => 'application/x-www-form-urlencoded');
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $reqbody = EstraierPure_Utility::cond_to_query(
            $cond, $depth, $this->wwidth, $this->hwidth, $this->awidth
        );
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads, $reqbody);
        if (!$res) {
            return null;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return null;
        }
        $lines = explode("\n", $res->body());
        if (count($lines) == 0) {
            return null;
        }
        $docs = array();
        $hints = array();
        $border = $lines[0];
        $isend = false;
        $lnum = 1;
        $llen = count($lines);
        $blen = strlen($border);
        while ($lnum < $llen) {
            $line = $lines[$lnum];
            $lnum++;
            if (strlen($line) >= $blen && strpos($line, $border) === 0) {
                if (substr($line, $blen) == ':END') {
                    $isend = true;
                }
                break;
            }
            if (strpos($line, "\t")) {
                list($key, $value) = explode("\t", $line, 2);
                $hints[$key] = $value;
            }
        }
        $snum = $lnum;
        while (!$isend && $lnum < $llen) {
            $line = $lines[$lnum];
            $lnum++;
            if (strlen($line) >= $blen && strpos($line, $border) === 0) {
                if ($lnum > $snum) {
                    $rdattrs = array();
                    $sb = '';
                    $rdvector = '';
                    $rlnum = $snum;
                    while ($rlnum < $lnum - 1) {
                        $rdline = trim($lines[$rlnum]);
                        $rlnum++;
                        if (strlen($rdline) == 0) {
                            break;
                        }
                        if (substr($rdline, 0, 1) == '%') {
                            $lidx = strpos($rdline, "\t");
                            if (strpos($rdline, '%VECTOR') === 0 && $lidx) {
                                $rdvector = substr($rdline, $lidx + 1);
                            }
                        } else {
                            if (strpos($rdline, '=')) {
                                list($key, $value) = explode('=', $rdline, 2);
                                $rdattrs[$key] = $value;
                            }
                        }
                    }
                    while ($rlnum < $lnum - 1) {
                        $rdline = $lines[$rlnum];
                        $rlnum++;
                        $sb .= $rdline . "\n";
                    }
                    $rduri = $rdattrs['@uri'];
                    $rdsnippet = $sb;
                    if ($rduri) {
                        $docs[] = &new EstraierPure_ResultDocument(
                            $rduri, $rdattrs, $rdsnippet, $rdvector
                        );
                    }
                }
                $snum = $lnum;
                if (substr($line, $blen) == ':END') {
                    $isend = true;
                }
            }
        }
        if (!$isend) {
            $null = null;
            return $null;
        }
        $result = &new EstraierPure_NodeResult($docs, $hints);
        return $result;
    }

    // }}}
    // {{{ other private methods

    /**
     * Set information of the node.
     *
     * @return  void
     * @access  private
     */
    function set_info()
    {
        $this->status = -1;
        if (!$this->url) {
            return;
        }
        $turl = $this->url . '/inform';
        $reqheads = array();
        if ($this->auth) {
            $reqheads['authorization'] = 'Basic ' . base64_encode($this->auth);
        }
        $res = &EstraierPure_Utility::shuttle_url($turl,
            $this->pxhost, $this->pxport, $this->timeout, $reqheads);
        if (!$res) {
            return;
        }
        $this->status = $res->status();
        if ($res->is_error()) {
            return;
        }
        $lines = explode("\n", $res->body());
        if (count($lines) == 0) {
            return;
        }
        $elems = explode("\t", $lines[0]);
        if (count($elems) != 5) {
            return;
        }
        $this->name = $elems[0];
        $this->label = $elems[1];
        $this->dnum = intval($elems[2]);
        $this->wnum = intval($elems[3]);
        $this->size = floatval($elems[4]);
        $llen = count($lines);
        if ($llen < 2) {
            return;
        }
        $lnum = 1;
        if ($lnum < $llen && strlen($lines[$lnum]) < 1) {
            $lnum++;
        }
        $this->admins = array();
        while ($lnum < $llen) {
            $line = $lines[$lnum];
            if (strlen($line) < 1) {
                break;
            }
            $this->admins[] = $line;
            $lnum++;
        }
        if ($lnum < $llen && strlen($lines[$lnum]) < 1) {
            $lnum++;
        }
        $this->users = array();
        while ($lnum < $llen) {
            $line = $lines[$lnum];
            if (strlen($line) < 1) {
                break;
            }
            $this->users[] = $line;
            $lnum++;
        }
        if ($lnum < $llen && strlen($lines[$lnum]) < 1) {
            $lnum++;
        }
        $this->links = array();
        while ($lnum < $llen) {
            $line = $lines[$lnum];
            if (strlen($line) < 1) {
                break;
            }
            $links = explode($line);
            if (count($links) == 3) {
                $this->links[] = $links;
            }
            $lnum++;
        }
    }

    // }}}
}

// }}}
// {{{ class EstraierPure_Response

/**
 * Class for HTTP response.
 *
 * @category    Web Services
 * @package     EstraierPure
 * @author      Ryusuke SEKIYAMA <rsky0711@gmail.com>
 * @version     Release: 0.6.0
 * @since       Class available since Release 0.0.1
 * @ignore
 */
class EstraierPure_Response
{
    // {{{ properties

    /**
     * The status code
     *
     * @var int
     * @access  private
     */
    var $code;

    /**
     * Headers of response
     *
     * @var array
     * @access  private
     */
    var $headers;

    /**
     * The entity body of response
     *
     * @var string
     * @access  private
     */
    var $body;

    // }}}
    // {{{ constructor

    /**
     * Create a response storage object.
     *
     * @param   int     $code   The status code.
     * @param   array   $heads  The hash of the headers.
     * @param   string  $body   The entity body of response,
     * @access  public
     */
    function EstraierPure_Response($code, $headers, $body)
    {
        $this->code = $code;
        $this->headers = $headers;
        $this->body = $body;
    }

    // }}}
    // {{{ getter methods

    /**
     * Determine success.
     *
     * @return  bool    True if the status code is 200, else false.
     * @access  public
     */
    function is_success()
    {
        return ($this->code == 200);
    }

    /**
     * Determine error.
     *
     * @return  bool    True if the status code is not 200, else false.
     * @access  public
     */
    function is_error()
    {
        return ($this->code != 200);
    }

    /**
     * Get the status code.
     *
     * @return  int     The status code of the response.
     * @access  public
     */
    function status()
    {
        return $this->code;
    }

    /**
     * Get the value of a header.
     *
     * @param   string  $name  The name of a header
     * @return  string  The value of the header.
     *                  If it does not exist, returns `false'.
     * @access  public
     */
    function header($name)
    {
        $name = strtolower($name);
        return (isset($this->heads[$name])) ? $this->heads[$name] : false;
    }

    /**
     * Get a hash of headers.
     *
     * @return  array   All response headers.
     * @access  public
     */
    function headers()
    {
        return $this->heads;
    }

    /**
     * Get the entity body of response
     *
     * @return  string  The entity body of response.
     * @access  public
     */
    function body()
    {
        return $this->body;
    }

    // }}}
}

// }}}

/*
 * Local variables:
 * mode: php
 * coding: iso-8859-1
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
// vim: set syn=php fenc=iso-8859-1 ai et ts=4 sw=4 sts=4 fdm=marker:
