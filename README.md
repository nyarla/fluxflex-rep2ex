# これは何？

PHPで書かれた2chブラウザ、[rep2](http://akid.s17.xrea.com/)の機能拡張版、

* [rep2expack](http://page2.skr.jp/rep2/)

の[fluxflex.com](http://www.fluxflex.com/)向けのパッケージです。

# 使い方

fluxflex.comのgithub importで、

* `git://github.com/nyarla/fluxflex-rep2ex.git`

を指定してインポートすれば大体使えます。

が、セキュリティの設定は甘々なので、その辺りは上記リポジトリをimportしたあと、
fluxflexのgit repository of your projectをcloneしてきて、
色々といじった方が良いかと思います。

# 修正点

fluxflexではどうも`REMOTE_ADDR`と`REMOTE_HOST`が特殊っぽく、
そのままのコードではその辺りが取得できなかったため、
一部のファイルに修正を加えています。

具体的には、`$_SERVER["REMOTE_ADDR"]`を使う代わりに`$_SERVER["HTTP_X_REAL_IP"]`を、
`$_SERVER["REMOTE_HOST"]`の代わりに`gethostbyaddr($_SERVER["HTTP_X_REAL_IP"])`を使うように修正しています。

# バージョンとか

現在このパッケージでは、

* rep2expack rev.111023.2200

を使用しています。

# 著作権

* [rep2 is created by Aki](http://akid.s17.xrea.com/)
* [rep2expack is created by rsk](http://page2.skr.jp/rep2/)

* This package is created by Naoki Okamura (Nhyarla) *nyarla[ at ]thotep.net*

